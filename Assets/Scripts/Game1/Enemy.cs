using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game1
{

public class Enemy : MonoBehaviour
{
  Vector3 spawn_position;

  void Start()
  {
    spawn_position = transform.position;
  }

  void Update()
  {
    transform.position = spawn_position + transform.forward * Mathf.PingPong(Time.time * 2.0f, 10.0f);
  }
}

}
